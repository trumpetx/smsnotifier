package com.trumpetx.smsnotifier.gui;

import com.trumpetx.smsnotifier.MainApp;
import com.trumpetx.smsnotifier.services.TextManager;
import javafx.application.Platform;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;

@PetiteBean
public class SwingSystemTray {

    private static final Logger LOG = LoggerFactory.getLogger(SwingSystemTray.class);

    @PetiteInject
    private TextManager textManager;
    @PetiteInject
    private MainStageAccessor mainStageAccessor;

    public boolean doSystemTray() {
        if (!Platform.isFxApplicationThread()) {
            throw new IllegalStateException(SystemTray.class.getName()
                    + " can only be create within the JavaFX application thread");
        }
        if (SystemTray.isSupported()) {
            SystemTray tray = SystemTray.getSystemTray();
            URL icon = null;
            try {
                icon = SwingSystemTray.class.getResource("/images/ApplicationIcon_16_16.png");
            } catch (RuntimeException e) {
                LOG.warn("Unable to create a system tray entry.", e);
                return false;
            }
            Image image = Toolkit.getDefaultToolkit().getImage(icon);

            PopupMenu popup = new PopupMenu();

            final MenuItem hideOrShow = new MenuItem();
            hideOrShow.addActionListener(e -> performToggle(hideOrShow));
            popup.add(hideOrShow);

            MenuItem exit = new MenuItem(textManager.getText("exit"));
            exit.addActionListener(e -> {
                LOG.debug("Exiting via system tray.");
                System.exit(0);
            });
            popup.add(exit);

            TrayIcon trayIcon = new TrayIcon(image, textManager.getText("title"), popup);
            trayIcon.addActionListener(e -> performToggle(hideOrShow));

            trayIcon.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {

                }

                @Override
                public void mousePressed(MouseEvent e) {
                    hideOrShow.setLabel(mainStageAccessor.getMainStage().isShowing() ? textManager.getText("hide")
                            : textManager.getText("show"));
                }

                @Override
                public void mouseReleased(MouseEvent e) {

                }

                @Override
                public void mouseEntered(MouseEvent e) {

                }

                @Override
                public void mouseExited(MouseEvent e) {

                }
            });

            try {
                tray.add(trayIcon);
            } catch (Exception e) {
                LOG.warn("Unable to create a system tray.", e);
                return false;
            }

            return true;
        } else {
            LOG.info("System Tray not supported.  Unable to create a system tray.");
            return false;
        }
    }

    private void performToggle(MenuItem hide) {
        if (mainStageAccessor.getMainStage().isShowing()) {
            mainStageAccessor.hideMainStage();
        } else {
            mainStageAccessor.showMainStage();
        }
    }
}

