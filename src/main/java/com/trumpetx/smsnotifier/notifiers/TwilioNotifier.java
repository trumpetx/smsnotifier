package com.trumpetx.smsnotifier.notifiers;

import com.trumpetx.smsnotifier.util.Filter;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.jasypt.encryption.StringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TwilioNotifier implements INotifier {

    public static final String ACCOUNT_SID = "CIFlQpsjR0mXtO+CSUx1yhxFrW0eqmPtdAvKgHY1NS8FxTpDtGQIGQ2k+mGC5+7h";
    private static final String AUTH_TOKEN = "amXI3G/+qgY/eWvnA2h62hHQFHEFwiua/+aSJWSpNju/sTtZCeELAl6G0tZeEXYz";
    private static final String FROM = "HinqbDv3sdYyDwvmEpGMErhGZZTo21B4";
    private static final Logger LOG = LoggerFactory.getLogger(TwilioNotifier.class);

    private final String phoneNumber;
    private final String message;
    private final StringEncryptor encryptor;

    public TwilioNotifier(StringEncryptor encryptor, String phoneNumber, String message) {
        this.encryptor = encryptor;
        this.phoneNumber = Filter.phone(phoneNumber);
        this.message = message;
    }

    @Override
    public void sendNotification() throws TwilioRestException {

        if(LocalDate.now().isAfter(LocalDate.of(2015, 1, 1))){
            LOG.warn("Beta Software! - Go get a new app.  It'll be better, I promise.");
            return;
        }

        TwilioRestClient client = new TwilioRestClient(encryptor.decrypt(ACCOUNT_SID), encryptor.decrypt(AUTH_TOKEN));

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("From", encryptor.decrypt(FROM)));
        params.add(new BasicNameValuePair("To", phoneNumber));
        params.add(new BasicNameValuePair("Body", message));

        MessageFactory messageFactory = client.getAccount().getMessageFactory();
        LOG.debug("sid={}", messageFactory.create(params).getSid());
    }

}
