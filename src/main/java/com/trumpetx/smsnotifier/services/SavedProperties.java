package com.trumpetx.smsnotifier.services;

import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteDestroyMethod;
import jodd.petite.meta.PetiteInitMethod;
import jodd.petite.meta.PetiteInject;
import org.apache.commons.lang.StringUtils;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Map;
import java.util.Properties;

@PetiteBean
public class SavedProperties {

    private static final Logger LOG = LoggerFactory.getLogger(SavedProperties.class);
    private static final String PROP_LOCATION = System.getProperty("user.home") + File.separator + ".smsnotifier";

    private final StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
    private Properties buildProperties = new Properties(  );
    private EncryptableProperties currentProperties;
    @PetiteInject
    private TextManager textManager;

    @PetiteInitMethod
    public void init() {
        try
        {
            buildProperties.load( getClass().getResourceAsStream( "/build.properties" ) );
            LOG.info( "Build Properties:" );
            for(Map.Entry kvp : buildProperties.entrySet()){
                LOG.info("{}={}", kvp.getKey(), kvp.getValue());
            }
        }
        catch ( IOException e )
        {
            LOG.warn( "Unable to read build.properties", e );
        }
        getEncryptor().setPassword(textManager.getText("a.messagekey", 1, 'n'));
        getEncryptor().setAlgorithm("PBEWithMD5AndDES");
        getEncryptor().initialize();
    }

    /**
     * Lazily load properties from the filesystem if they exist.
     *
     * @return
     */
    public Properties getProperties() {
        synchronized (this) {
            if (currentProperties == null) {
                File p = new File(PROP_LOCATION);
                try {
                    currentProperties = new EncryptableProperties(getEncryptor());
                    currentProperties.load(new FileInputStream(p));
                } catch (IOException e) {
                    LOG.warn("Properties file not found, creating a blank one.");
                    try {
                        if (!p.createNewFile()) {
                            throw new IOException();
                        }
                    } catch (IOException e2) {
                        throw new RuntimeException("Unable to create properites file here: " + p.getAbsolutePath());
                    }
                }
            }
        }
        return currentProperties;
    }

    @PetiteDestroyMethod
    public void writeProperties() throws IOException {
        synchronized (this) {
            Properties p = new Properties();
            LOG.debug("Saving:");
            for (Map.Entry o : currentProperties.entrySet()) {
                String value = String.valueOf(o.getValue());
                if (!StringUtils.startsWith(value, "ENC")) {
                    p.setProperty(String.valueOf(o.getKey()), "ENC(" + getEncryptor().encrypt(String.valueOf(value)) + ")");
                } else {
                    p.setProperty(String.valueOf(o.getKey()), String.valueOf(o.getValue()));
                }
                LOG.debug("{}={}", o.getKey(), p.getProperty((String) o.getKey()));
            }
            p.store(new FileOutputStream(PROP_LOCATION), "SMS Notifier Properties");
            currentProperties = null;
        }
    }

    public String getProperty(String key) {
        synchronized (this) {
            if(buildProperties.containsKey( key )){
                return buildProperties.getProperty( key );
            }
            return getProperties().getProperty(key);
        }
    }

    public boolean getBoolProperty(String key) {
        return getBoolProperty(key, false);
    }

    public boolean getBoolProperty(String key, boolean defaultValue) {
        synchronized (this) {
            String value = getProperty(key);
            if (value == null) {
                return defaultValue;
            }
            return Boolean.parseBoolean(value);
        }
    }

    public LocalDateTime getTime(String key) {
        synchronized (this) {
            try {
                return LocalDateTime.parse(getProperty(key));
            } catch (NullPointerException | DateTimeParseException e) {
                return null;
            }
        }
    }

    public void setProperty(String key, Object value) {
        synchronized (this) {
            if (value != null) {
                getProperties().setProperty(key, String.valueOf(value));
            } else {
                getProperties().remove(key);
            }
        }
        LOG.debug("{}={}", key, value);
    }

    public StandardPBEStringEncryptor getEncryptor() {
        return encryptor;
    }

}
