package com.trumpetx.smsnotifier.controllers;

public interface IPresentable {
    public void present();
}
