package com.trumpetx.smsnotifier.notifiers;

import jodd.mail.Email;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;
import jodd.mail.SmtpSslServer;
import org.jasypt.encryption.StringEncryptor;

public class EmailNotifier implements INotifier {

    private static final String PWRD = "rZ6q8r46t2O7l2BPlf4+Pp6sxx2FUyUX";
    private static final String FROM = "wowsmsnotifier@trumpetx.com";

    private final String title;
    private final String message;
    private final String to;
    private final StringEncryptor encryptor;

    public EmailNotifier(StringEncryptor encryptor, String to, String title, String message) {
        this.encryptor = encryptor;
        this.to = to;
        this.title = title;
        this.message = message;
    }

    @Override
    public void sendNotification() {

        Email email = Email.create()
                .from(FROM)
                .to(to)
                .subject(title)
                .addText(message);

        SmtpServer smtpServer = new SmtpSslServer("smtp.gmail.com", "wowsmsnotifier@trumpetx.com", encryptor.decrypt(PWRD));
        SendMailSession session =  smtpServer.createSession();
        session.open();
        session.sendMail(email);
        session.close();
        session.close();
    }


}
