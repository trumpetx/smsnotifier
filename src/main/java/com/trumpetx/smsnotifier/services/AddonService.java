package com.trumpetx.smsnotifier.services;

import com.trumpetx.smsnotifier.Constants;
import com.trumpetx.smsnotifier.notifiers.DesktopNotifier;
import com.trumpetx.smsnotifier.util.Validate;
import jodd.io.FileUtil;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Arrays;
import java.util.List;

@PetiteBean
public class AddonService {

    private static final Logger LOG = LoggerFactory.getLogger(AddonService.class);

    public static final String ADDON_FOLDERNAME = "SMSNotifier";
    public static final String INTERFACE_ADDONS_FOLDERNAME = "Interface" + File.separator + "AddOns";
    public static final String SMS_NOTIFIER_TOC = "SMSNotifier.toc";
    public static final List<String> ADDON_FILES = Arrays.asList(SMS_NOTIFIER_TOC, "SMSNotifier.lua", "SMSNotifier.xml", "lfg.tga", "pet.tga", "pvp.tga");
    private static final int DEV_VERSION = 0xBEEF;
    private static final String DEV_VERSION_STR = String.valueOf( DEV_VERSION );


    @PetiteInject
    private SavedProperties properties;

    public void installAddon(boolean currentThread) {
        if (currentThread) {
            installAddon();
        } else {
            new Thread((() -> installAddon()), "AddonInstaller").start();
        }
    }

    private boolean installAddon() {
        File addonsDir = getAddonsDir();

        if (Validate.writableDir(getAddonsDir())) {
            try {
                double currentVersion = getCurrentVersion();
                double installedVersion = getInstalledVersion();
                LOG.info("Addon installed version: {}, Current version: {}", installedVersion, currentVersion);
                if (currentVersion > installedVersion || currentVersion == DEV_VERSION || installedVersion == DEV_VERSION) {
                    File addonDir = new File(addonsDir.getAbsolutePath() + File.separator + ADDON_FOLDERNAME);
                    addonDir.mkdirs();
                    for (String file : ADDON_FILES) {
                        FileUtil.writeStream(addonDir.getAbsolutePath() + File.separator + file, AddonService.class.getResourceAsStream('/' + ADDON_FOLDERNAME + '/' + file));
                    }
                    String msg = String.format("Successfully installed SMSNotifier addon v%.2f!", currentVersion);
                    new DesktopNotifier("Addon Installation", msg).sendNotification();
                    LOG.info(msg);
                } else {
                    LOG.debug("SMSNotifier addon is already installed.");
                }
                return true;
            } catch (IOException e) {
                LOG.error("Error installing SMSNotifier addon", e);
            }
        } else if (addonsDir != null) {
            LOG.error("Addons Directory ({}) is not writable.", addonsDir.getAbsolutePath());
        }
        LOG.error("Unable to install SMSNotifier addon.");
        return false;
    }

    private double getCurrentVersion() {
        try (InputStream is = AddonService.class.getResourceAsStream('/' + ADDON_FOLDERNAME + '/' + SMS_NOTIFIER_TOC)) {
            return getVersion(is);
        } catch (IOException e) {
            LOG.error("Error getting current version.", e);
        }
        return 0d;
    }

    private double getVersion(InputStream is) throws IOException {
        double version = 0d;
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            if (StringUtils.containsIgnoreCase(line, "version")) {
                String versionString = StringUtils.substringAfter(line, ":").trim();
                if(DEV_VERSION_STR.equals( versionString )){
                    return (double) DEV_VERSION;
                }
                try {
                    version = Double.parseDouble(versionString);
                } catch (NumberFormatException e) { }
                break;
            }
        }
        return version;
    }

    private double getInstalledVersion() {
        File addonsDir = getAddonsDir();
        if (addonsDir != null) {
            File addonToc = new File(addonsDir.getAbsolutePath() + File.separator + ADDON_FOLDERNAME + File.separator + SMS_NOTIFIER_TOC);
            if (Validate.readableFile(addonToc)) {
                try (InputStream is = new FileInputStream(addonToc)) {
                    return getVersion(is);
                } catch (IOException e) {
                    LOG.error("Error getting installed version.", e);
                }
            }
    }
        return 0d;
    }

    private File getAddonsDir() {
        String wowDirLoc = properties.getProperty(Constants.WOWDIR_CFG);
        try {
            if (null != wowDirLoc) {
                File addonsDir = new File(wowDirLoc + File.separator + INTERFACE_ADDONS_FOLDERNAME);
                if (!addonsDir.exists()) {
                    addonsDir.mkdirs();
                }
                if (Validate.readableDir(addonsDir)) {
                    return addonsDir;
                }
            }
        } catch (RuntimeException e) {
            LOG.warn("Unable to write to or create AddOns folder.");
        }

        LOG.info("No AddOns directory available");
        return null;
    }


}
