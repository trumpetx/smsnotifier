package com.trumpetx.smsnotifier;

import com.sun.javafx.runtime.VersionInfo;
import com.trumpetx.smsnotifier.gui.MainStageAccessor;
import com.trumpetx.smsnotifier.runnable.Monitor;
import com.trumpetx.smsnotifier.services.AddonService;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import jodd.log.impl.Slf4jLoggerFactory;
import jodd.petite.PetiteContainer;
import jodd.petite.config.AutomagicPetiteConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Initialize our JavaFX app and our Petite DI Container
 */
public class MainApp extends Application {

    private static final Logger LOG = LoggerFactory.getLogger(MainApp.class);
    private PetiteContainer petiteContainer;

    public static void main(String[] args) {
        LOG.info("Operating System: {}", System.getProperty("os.name"));
        LOG.info("JavaFX Version: {}", VersionInfo.getRuntimeVersion());
        LOG.info("Java Version: {}", System.getProperty("java.version"));
        try {
            launch(args);
        } catch (Exception e) {
            LOG.debug("", e);
            LOG.error("Application Error: {}", e.getMessage());
            System.exit(99);
        }
    }

    @Override
    public void start(final Stage stage)
            throws Exception {
        try {
            jodd.log.LoggerFactory.setLoggerFactory(new Slf4jLoggerFactory());
            petiteContainer = new PetiteContainer();
            petiteContainer.addSelf();
            new AutomagicPetiteConfigurator().configure(petiteContainer);
            Runtime.getRuntime().addShutdownHook(new Thread(() -> petiteContainer.shutdown()));



        } catch (Exception e) {
            LOG.debug("", e);
            LOG.error("Error during startup: {}", e.getMessage());
            System.exit(99);
        }

        Platform.runLater(() -> {
            petiteContainer.getBean(MainStageAccessor.class).init(stage);

            // Start threads after init for popups
            new Thread(petiteContainer.getBean(Monitor.class), "MonitorThread").start();
            petiteContainer.getBean(AddonService.class).installAddon(false);
        });
    }
}
