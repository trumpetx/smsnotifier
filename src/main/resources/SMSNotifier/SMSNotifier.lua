--
-- Do the notification
--
function SMSNotifier_DoNotify(type, img)
    print("SMSNotifier detected a " .. type .." pop.")

    local f = CreateFrame("Frame",nil,UIParent)
    f:SetFrameStrata("BACKGROUND")
    f:SetWidth(256)
    f:SetHeight(256)

    local t = f:CreateTexture(nil,"BACKGROUND")
    t:SetTexture("Interface/AddOns/SMSNotifier/" .. img .. ".tga")
    t:SetAllPoints(f)
    f:SetFrameStrata("TOOLTIP")
    f.texture = t

    f:SetPoint("TOPLEFT", 0, 0)
    f:Show()
    TakeScreenshot()
    C_Timer.After(.4, function()
        f:Hide()
    end)
end

--
-- Main addon callback method 'OnEvent'
--
function SMSNotifier_OnEvent(self, event, arg1, ...)
    if event == "ADDON_LOADED" and arg1 == "SMSNotifier" then
        SMSNotifier_OnLoad(self);
    elseif (not smsEnabled) then
        -- print("SMS Notifier not enabled - no notification sent.")
        return
    elseif event == "LFG_PROPOSAL_SHOW" and smsQueue then
        proposalExists, id, typeID, subtypeID, name, texture, role, hasResponded, nonsenseValue, completedEncounters, numMembers, isLeader, isSomethingElse, totalEncounters = GetLFGProposal();
        if proposalExists then
            SMSNotifier_DoNotify("LFG/LFR/Scenario", "lfg");
        end
    elseif event == "UPDATE_BATTLEFIELD_STATUS" and smsBgQueue then
        for i = 1, 3 do
            status, mapName, instanceID = GetBattlefieldStatus(i)
            if status == "confirm" then
                SMSNotifier_DoNotify("pvp", "pvp");
                return
            end
        end
    elseif event == "PET_BATTLE_QUEUE_PROPOSE_MATCH" and smsPetQueue then
        SMSNotifier_DoNotify("pet battle", "pet");
    end
end

--
-- Display addon status
--
function printQueueVars()
    if (smsEnabled) then
        printBoolVar("LFG/LFR/Scenario notifications are", smsQueue)
        printBoolVar("Battleground notifications are", smsBgQueue)
        printBoolVar("Pet Battle notifications are", smsPetQueue)
    else
        printBoolVar("Addon is", smsEnabled)
    end
end

--
-- Print Boolean variable as enabled/disabled
--
function printBoolVar(label, value)
    local v = value == nil and "nil" or (value and "enabled" or "disabled")
    print(string.format("%s %s", label, v))
end

--
-- Create a checkbox
--
function SMSNotifier_CreateCheckbox_Full(panel, isChecked, checkboxName, label, x, y, fn)
    local checkbox = CreateFrame("CheckButton", checkboxName .. "_GlobalName", panel, "ChatConfigCheckButtonTemplate")
    checkbox:SetChecked(isChecked)
    checkbox:SetPoint("TOPLEFT", x, y)
    getglobal(checkbox:GetName() .. 'Text'):SetText(label);
    checkbox:SetScript("OnClick", fn)
    return checkbox
end

--
-- Create common checkboxes w/this
--
function SMSNotifier_CreateCheckbox(panel, isChecked, checkboxName, label, key, x, y)
    local c = SMSNotifier_CreateCheckbox_Full(panel, isChecked, checkboxName, "  Enable " .. label .. " Notifications", x, y, function(self) SMSNotifier_DoMessage(key); end)
    return c
end

--
-- OnLoad for addon, init checkboxes
--
function SMSNotifier_OnLoad(panel)
    panel.name = "SMS Notifier"
    InterfaceOptions_AddCategory(panel)
    header = panel:CreateFontString("headerText", "ARTWORK", "GameFontNormal")
    header:SetPoint("TOPLEFT", 15, -20)
    headerText:SetText("SMS Notifier Options")
    smsEnabledCheck = SMSNotifier_CreateCheckbox_Full(panel, smsEnabled, "smsEnabledCheck", "  Enable the WoW SMS Notifer Addon", 15, -50, function(self)
        if self:GetChecked() then
            SMSNotifier_DoMessage("on")
        else
            SMSNotifier_DoMessage("off")
        end
    end)
    smsQueueCheck = SMSNotifier_CreateCheckbox(panel, smsQueue, "smsQueueCheck", "LFG/LFR/Scenario", "queue", 15, -70)
    smsPetQueueCheck = SMSNotifier_CreateCheckbox(panel, smsPetQueue, "smsPetQueueCheck", "Pet Battle", "pet", 15, -90)
    smsBgQueueCheck = SMSNotifier_CreateCheckbox(panel, smsBgQueue, "smsBgQueueCheck", "PvP", "pvp", 15, -110)
end

--
-- Show Messages, Set Variables
--
function SMSNotifier_DoMessage(msg)
    msg = msg:lower()
    if msg == 'help' then
        print([[ SMS Notifier Help:
		/sms /smsnotifier - Displays the current notification status
		/sms (on/off) - Disable or enable the SMS Notifier Addon
		/sms queue - Disable or enable LFG/LFR/Scenario notifications
		/sms pet - Disable or enable Pet Battle notifications
		/sms pvp - Disable or enable Player versus Player notifications
		/sms test - Sends a test LFG notification
		/sms help - Show this message. ]]);
    elseif msg == 'queue' or msg == 'lfg' then
        smsQueue = not smsQueue
        smsQueueCheck:SetChecked(smsQueue);
        print('LFG/LFR/Scenario notifications ' .. (smsQueue and 'enabled' or 'disabled'))
    elseif msg == 'pet' or msg == 'pets' then
        smsPetQueue = not smsPetQueue
        smsPetQueueCheck:SetChecked(smsPetQueue);
        print('Pet Battle notifications ' .. (smsPetQueue and 'enabled' or 'disabled'))
    elseif msg == 'off' or msg == 'on' then
        smsEnabled = not smsEnabled
        smsEnabledCheck:SetChecked(smsEnabled)
        print('SMS Notifier is ' .. (smsEnabled and 'enabled' or 'disabled'))
    elseif msg == 'pvp' then
        smsBgQueue = not smsBgQueue
        smsBgQueueCheck:SetChecked(smsBgQueue)
        print('PvP notifications ' .. (smsBgQueue and 'enabled' or 'disabled'))
    elseif msg == 'test' then
        SMSNotifier_DoNotify("test", "lfg")
    elseif msg =='' then
        printQueueVars()
    else
        print('Invalid Command')
    end
    return true;
end

--
-- Slash Commands
--
SLASH_SMSNotifier1, SLASH_SMSNotifier2 = '/smsnotifier', '/sms'
SlashCmdList["SMSNotifier"] = SMSNotifier_DoMessage