package com.trumpetx.smsnotifier.notifiers;

import com.trumpetx.smsnotifier.Constants;
import com.trumpetx.smsnotifier.services.SavedProperties;
import com.trumpetx.smsnotifier.services.TextManager;
import com.trumpetx.smsnotifier.util.Filter;
import com.trumpetx.smsnotifier.util.Validate;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@PetiteBean
public class NotiferFactoryService {

    private static final Logger LOG = LoggerFactory.getLogger(NotiferFactoryService.class);
    @PetiteInject
    private SavedProperties properties;
    @PetiteInject
    private TextManager textManager;

    private List<INotifier> getNotifiers(String msg) {
        List<INotifier> senders = new ArrayList<>();
        synchronized (properties) {
            if (isPhoneVerified()) {
                senders.add(getPhoneNotifier(msg, textManager.getText("queue.pop.notification.title")));
            }

            if (properties.getBoolProperty("desktopNotifierEnabled", true)) {
                senders.add(new DesktopNotifier(textManager.getText("queue.pop.notification.title"), msg));
            }

            String email = properties.getProperty(Constants.EMAIL_CFG);
            if (Validate.email(email)) {
                senders.add(new EmailNotifier(properties.getEncryptor(), email, textManager.getText("queue.pop.notification.title"), msg));
            }
        }
        return senders;
    }

    public void sendNotification(String msg) throws Exception {
        for (INotifier notifier : getNotifiers(msg)) {
            notifier.sendNotification();
        }
    }

    private boolean isPhoneVerified() {
        LocalDateTime verified = properties.getTime(Constants.SMS_VERIFIED_CFG);
        return verified != null && verified.isBefore(LocalDateTime.now()) && Validate.phone(properties.getProperty(Constants.PHONE_CFG));
    }

    public INotifier getPhoneNotifier(String message, String optionalTitle){
        String carrier = properties.getProperty(Constants.CARRIER_CFG);
        String phone = Filter.phone(properties.getProperty(Constants.PHONE_CFG));
        if(Constants.CARRIER_EMAIL_TEMPLATES.containsKey(carrier)){
            return new EmailNotifier(properties.getEncryptor(), String.format(Constants.CARRIER_EMAIL_TEMPLATES.get(carrier), phone), optionalTitle, message);
        } else {
            return new TwilioNotifier(properties.getEncryptor(), phone, message);
        }
    }
}
