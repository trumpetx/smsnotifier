package com.trumpetx.smsnotifier.util;

import org.apache.commons.lang.StringUtils;

public class Filter {
    public static String phone(String phoneNumber) {
        StringBuilder sb = new StringBuilder();
        if (phoneNumber != null) {
            for (char c : phoneNumber.toCharArray()) {
                if (Character.isDigit(c)) {
                    sb.append(c);
                }
            }
        }

        return sb.toString();
    }

    public static String email(String email) {
        return StringUtils.trimToEmpty(email);
    }
}
