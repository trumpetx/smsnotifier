package com.trumpetx.smsnotifier.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import com.trumpetx.smsnotifier.Constants;
import com.trumpetx.smsnotifier.notifiers.DesktopNotifier;
import com.trumpetx.smsnotifier.notifiers.NotiferFactoryService;
import com.trumpetx.smsnotifier.runnable.WowFinder;
import com.trumpetx.smsnotifier.services.AddonService;
import com.trumpetx.smsnotifier.util.Filter;
import com.trumpetx.smsnotifier.util.Validate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.stage.DirectoryChooser;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

@PetiteBean
public class FXMLController extends AbstractController {

    @PetiteInject
    private AddonService addonService;
    @PetiteInject
    private NotiferFactoryService notiferFactoryService;

    @FXML
    private Label label;
    @FXML
    private CheckBox enableToggle;
    @FXML
    private CheckBox enableEmailToggle;
    @FXML
    private CheckBox enableDesktopToggle;
    @FXML
    private CheckBox startupToggle;
    @FXML
    private TextField phoneEntry;
    @FXML
    private TextField emailEntry;
    @FXML
    private Button testButton;
    @FXML
    private TextField validationCode;
    @FXML
    private Group verifyGroup;
    @FXML
    private TextField wowDir;
    @FXML
    private ComboBox<String> carrierCombo;

    @FXML
    private void handleButtonAction(ActionEvent event) {
        try {
            String phoneNumber = properties.getProperty(Constants.PHONE_CFG);
            if (phoneNumber != null) {
                String verificationCode = RandomStringUtils.random(6, true, true).toUpperCase();
                properties.setProperty(Constants.VERIFICATION_CODE_CFG, verificationCode);
                notiferFactoryService.getPhoneNotifier(getText("verification.message", verificationCode), textManager.getText("verification")).sendNotification();
                label.setText(getText("message.sent"));
                verifyGroup.setVisible(true);
            }
        } catch (Exception e) {
            LOG.error("", e);
            label.setText(getText("send.error"));
        }
    }

    @FXML
    private void handleVerificationLite(KeyEvent event) {
        if (StringUtils.equalsIgnoreCase(StringUtils.trim(validationCode.getText()), properties.getProperty(Constants.VERIFICATION_CODE_CFG))) {
            properties.setProperty(Constants.SMS_VERIFIED_CFG, LocalDateTime.now());
            testButton.setVisible(false);
            verifyGroup.setVisible(false);
            label.setText(getText("valid.verification.code"));
        }
    }

    @FXML
    private void handleVerification(ActionEvent event) {
        handleVerificationLite(null);
        if (!isVerified()) {
            label.setText(getText("invalid.verification.code"));
        }
    }

    @FXML
    private void onEnableChange(ActionEvent event) {
        properties.setProperty(Constants.SMS_ENABLED_CFG, enableToggle.isSelected());
    }

    @FXML
    private void onEnableEmailChange(ActionEvent event) {
        properties.setProperty(Constants.EMAIL_ENABLED_CFG, enableEmailToggle.isSelected());
    }

    @FXML
    private void onEnableDesktopChange(ActionEvent event) {
        properties.setProperty(Constants.DESKTOP_ENABLED_CFG, enableDesktopToggle.isSelected());
    }

    @FXML
    private void onStartupChange(ActionEvent event) {
        properties.setProperty(Constants.START_ON_STARTUP_CFG, startupToggle.isSelected());
    }

    @FXML
    private void onPhoneEntryChange(KeyEvent event) {
        setPhone(phoneEntry.getText().trim());
    }


    @FXML
    private void onPhoneEntryChange2(ActionEvent event) {
        setPhone(phoneEntry.getText().trim());
    }

    private void setPhone(String phoneNumber) {
        synchronized(phoneEntry) {
            boolean isValid = Validate.phone(phoneNumber);
            if (isValid || StringUtils.isBlank(phoneNumber)) {
                String existingPhone = properties.getProperty(Constants.PHONE_CFG);
                properties.setProperty(Constants.PHONE_CFG, phoneNumber);
                phoneEntry.setText(phoneNumber);
                if (!StringUtils.equals(Filter.phone(existingPhone), Filter.phone(phoneNumber))) {
                    properties.setProperty(Constants.SMS_VERIFIED_CFG, null);
                    reset();
                }
                if (testButton.isVisible()) {
                    testButton.requestFocus();
                }
            }
            testButton.setDisable(!isValid);
        }
    }

    @FXML
    private void onEmailEntryChanged(ActionEvent event) {
        String email = Filter.email(emailEntry.getText());
        boolean isValid = Validate.email(email);
        if (isValid) {
            properties.setProperty(Constants.EMAIL_CFG, email);
            emailEntry.setText(email);
            phoneEntry.requestFocus();
        } else {
            properties.setProperty(Constants.EMAIL_CFG, null);
            label.setText(getText("invalid.email"));
        }
    }

    @FXML
    private void chooseWowDir() {
        DirectoryChooser fileChooser = new DirectoryChooser();
        fileChooser.setTitle(getText("select.directory"));
        File selectedDirectory = fileChooser.showDialog(mainStageAccessor.getMainStage());
        if (!Validate.readableDir(selectedDirectory)) {
            label.setText(getText("invalid.wow.dir"));
        } else {
            setWowDir(selectedDirectory.getAbsolutePath());
        }
    }

    @FXML
    private void carrierSelected(){
        properties.setProperty(Constants.CARRIER_CFG, carrierCombo.getValue());
    }

    private void setWowDir(String dirLoc) {
        properties.setProperty(Constants.WOWDIR_CFG, dirLoc);
        wowDir.setText(dirLoc);
    }

    private void reset() {
        enableToggle.setSelected(properties.getBoolProperty(Constants.SMS_ENABLED_CFG));
        enableEmailToggle.setSelected(properties.getBoolProperty(Constants.EMAIL_ENABLED_CFG));
        enableDesktopToggle.setSelected(properties.getBoolProperty(Constants.DESKTOP_ENABLED_CFG));
        startupToggle.setSelected(properties.getBoolProperty(Constants.START_ON_STARTUP_CFG));
        setPhone(properties.getProperty(Constants.PHONE_CFG));
        emailEntry.setText(StringUtils.trimToEmpty(properties.getProperty(Constants.EMAIL_CFG)));
        boolean verified = isVerified();
        testButton.setVisible(!verified);
        carrierCombo.setVisible(!verified);
        carrierCombo.setValue(properties.getProperty(Constants.CARRIER_CFG));
        verifyGroup.setVisible(false);
        if (properties.getProperty(Constants.WOWDIR_CFG) == null || !Validate.readableDir(new File(properties.getProperty(Constants.WOWDIR_CFG)))) {
            new Thread(new WowFinder() {
                @Override
                public void callback(File wowFolder) {
                    new DesktopNotifier(getText("configuration"), getText("wowdir.found")).sendNotification();
                    setWowDir(wowFolder.getAbsolutePath());
                    addonService.installAddon(false);
                }
            }).start();
        } else {
            setWowDir(properties.getProperty(Constants.WOWDIR_CFG));
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle bundle) {
        super.initialize(url, bundle);
        carrierCombo.setItems(new ObservableListWrapper<>(Constants.CARRIERS));
        reset();
    }

    @Override
    public String view() {
        return "Scene";
    }

    private boolean isVerified() {
        LocalDateTime verified = properties.getTime(Constants.SMS_VERIFIED_CFG);
        return verified != null && verified.isBefore(LocalDateTime.now()) && Validate.phone(properties.getProperty(Constants.PHONE_CFG));
    }

}
