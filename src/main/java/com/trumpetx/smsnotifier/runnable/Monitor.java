package com.trumpetx.smsnotifier.runnable;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.trumpetx.smsnotifier.Constants;
import com.trumpetx.smsnotifier.notifiers.DesktopNotifier;
import com.trumpetx.smsnotifier.notifiers.NotiferFactoryService;
import com.trumpetx.smsnotifier.services.SavedProperties;
import com.trumpetx.smsnotifier.services.TextManager;
import com.trumpetx.smsnotifier.util.Validate;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteDestroyMethod;
import jodd.petite.meta.PetiteInject;
import jodd.petite.scope.ProtoScope;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@PetiteBean(scope = ProtoScope.class)
public class Monitor implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(Monitor.class);
    private static final Map<EncodeHintType, ErrorCorrectionLevel> HINT_MAP = Collections.unmodifiableMap(new HashMap<EncodeHintType, ErrorCorrectionLevel>() {{
        put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
    }});

    @PetiteInject
    private NotiferFactoryService notifier;
    @PetiteInject
    private SavedProperties properties;
    @PetiteInject
    private TextManager textManager;

    private boolean closed = false;
    private Thread t;
    private long lastPop = System.currentTimeMillis();

    public static File lastFileModified(File fl) {
        File[] files = fl.listFiles(file -> file.isFile());
        long lastMod = Long.MIN_VALUE;
        File choice = null;
        for (File file : files) {
            if ((file.getName().endsWith(".jpg") || file.getName().endsWith(".jpeg")) && file.lastModified() > lastMod) {
                choice = file;
                lastMod = file.lastModified();
            }
        }
        return choice;
    }

    public String readQRCode(File file, Map hintMap) throws IOException, NotFoundException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(file)))));
        Result qrCodeResult = new MultiFormatReader().decode(binaryBitmap, hintMap);
        return qrCodeResult.getText();
    }

    @Override
    public void run() {
        try {
            t = Thread.currentThread();
            while (true) {
                File screenshotDir = new File(properties.getProperty(Constants.WOWDIR_CFG) + File.separator + "Screenshots");
                if (Validate.readableDir(screenshotDir)) {
                    File last = lastFileModified(screenshotDir);
                    long currentPop = last == null ? 0L : last.lastModified();


                    if (lastPop < currentPop) {
                        lastPop = currentPop;
                        try{
                            String qrCodeText = StringUtils.trimToEmpty(readQRCode(last, HINT_MAP)).toLowerCase();
                            if(qrCodeText.contains("lfg")){
                                qrCodeText = textManager.getText("lfg.pop");
                            } else if(qrCodeText.contains("pet")) {
                                qrCodeText = textManager.getText("pet.pop");
                            } else if(qrCodeText.contains("bg")){
                                qrCodeText = textManager.getText("pvp.pop");
                            } else {
                                throw NotFoundException.getNotFoundInstance();
                            }

                            LOG.info("Pop:{}", qrCodeText);
                            notifier.sendNotification(qrCodeText);

                            // Only delete QR coded images
                            try {
                                last.delete();
                            } catch (RuntimeException e) {
                            }
                        } catch (NotFoundException nfe){
                            LOG.info("A screenshot was detected, but no QR code was inside.  No queue, no delete.");
                        }
                    } else {
                        LOG.debug("Last Pop: {}", lastPop);
                    }

                    Thread.sleep(5000L);
                    if (closed) {
                        return;
                    }
                    continue;
                }


                Thread.sleep(30000L);
                if (closed) {
                    return;
                }
            }
        } catch (InterruptedException e) {
            LOG.debug("Interrupted");
        } catch (Exception e) {
            LOG.error("", e);
            new DesktopNotifier(textManager.getText("queue.monitor.error"), e.getMessage());
        }
    }

    @PetiteDestroyMethod
    public synchronized void close() throws IOException {
        closed = true;
        if (t != null) {
            t.interrupt();
            t = null;
        }
    }

}
