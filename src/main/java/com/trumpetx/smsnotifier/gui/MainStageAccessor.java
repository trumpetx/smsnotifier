package com.trumpetx.smsnotifier.gui;

import com.trumpetx.smsnotifier.Constants;
import com.trumpetx.smsnotifier.controllers.FXMLController;
import com.trumpetx.smsnotifier.services.SavedProperties;
import com.trumpetx.smsnotifier.services.TextManager;
import com.trumpetx.smsnotifier.util.PlatformUtil;
import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

@PetiteBean
public class MainStageAccessor {

    private static final String APP_ICON = "ApplicationIcon_%s.png";
    private static final List<String> APP_ICON_SIZES = Arrays.asList("48_48", "32_32","100_100");
    private static final Logger LOG = LoggerFactory.getLogger(MainStageAccessor.class);

    @PetiteInject
    private SwingSystemTray swingSystemTray;
    @PetiteInject
    private SavedProperties properties;
    @PetiteInject
    private TextManager textManager;
    @PetiteInject
    private FXMLController fxmlController;

    private Stage mainStage;
    private Stage hiddenStage = new Stage(StageStyle.UTILITY);

    public MainStageAccessor() {
        hiddenStage.setWidth(1);
        hiddenStage.setHeight(1);
        hiddenStage.setResizable(false);
        hiddenStage.setOpacity(0D);
        hiddenStage.hide();
    }

    public void init(Stage ms) {
        this.mainStage = ms;
        addIconsToStage(mainStage);

        mainStage.setTitle(textManager.getText("title"));
        mainStage.setOnCloseRequest(event -> {
            if (properties.getBoolProperty(Constants.HIDE_ON_CLOSE_CFG) && SystemTray.isSupported()) {
                hideMainStage();
            } else {
                LOG.debug("Exiting via Close Request.");
                System.exit(0);
            }
        });
        mainStage.iconifiedProperty().addListener((observableValue, aBoolean, aBoolean2) -> {
            Boolean value = observableValue.getValue();
            if (properties.getBoolProperty(Constants.HIDE_ON_MINIMIZE_CFG, true) && value) {
                LOG.debug("Window Minimized");
                hideMainStage();
            } else {
                LOG.debug("Window Restored");
            }
        });

        if (!properties.getBoolProperty(Constants.STARTUP_MINIMIZED_CFG)) {
            hiddenStage.show();
            fxmlController.present();
            FadeTransition fadeSplash = new FadeTransition(Duration.seconds(1), mainStage.getScene().getRoot());
            fadeSplash.setFromValue(0.0);
            fadeSplash.setToValue(1.0);
            fadeSplash.setOnFinished(actionEvent -> {
                mainStage.show();
                hiddenStage.hide();
            });
            fadeSplash.play();
        } else {
            hideMainStage();
        }

        swingSystemTray.doSystemTray();

        LOG.debug("GUI Launched");
    }

    public void hideMainStage() {
        Platform.runLater(() -> {
            if (SystemTray.isSupported()) {
                LOG.debug("Hiding Main Stage");
                hiddenStage.show();
                mainStage.hide();
            }
        });
    }

    public void showMainStage() {
        Platform.runLater(() -> {
            LOG.debug("Showing Main Stage");
            // Make sure that the window is not minimized when it returns from being hidden
            mainStage.setIconified(false);
            mainStage.show();
            hiddenStage.close();
        });
    }

    public void setScene(Scene view) {
        mainStage.setScene(view);
        if (!mainStage.isShowing()) {
            showMainStage();
        }
    }

    public void addIconsToStage(Stage stage) {
        for (String icon : APP_ICON_SIZES) {
            try (InputStream is = MainStageAccessor.class.getResourceAsStream("/images/" + String.format(APP_ICON, icon))) {
                stage.getIcons().add(new javafx.scene.image.Image(is));
            } catch (IOException e) {
                LOG.debug("", e);
                LOG.error("Unable to locate icon image for taskbar.");
            }
        }
        if(PlatformUtil.isMac()) {
            com.apple.eawt.Application application = com.apple.eawt.Application.getApplication();
            java.awt.Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/ApplicationIcon_200_200.png"));
            application.setDockIconImage(image);
        }
    }

    public Stage getMainStage() {
        return mainStage;
    }
}
